﻿namespace MyPaint
{
    partial class FormPenSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.labelChooseColor = new System.Windows.Forms.Label();
            this.labelChooseWidth = new System.Windows.Forms.Label();
            this.trackBarChooseWidth = new System.Windows.Forms.TrackBar();
            this.buttonApply = new System.Windows.Forms.Button();
            this.buttonCansel = new System.Windows.Forms.Button();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarChooseWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MV Boli", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(71, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(140, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Найстройки пера";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MV Boli", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Выберите цвет пера:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MV Boli", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Выберите размер пера:";
            // 
            // labelChooseColor
            // 
            this.labelChooseColor.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelChooseColor.Location = new System.Drawing.Point(137, 56);
            this.labelChooseColor.Name = "labelChooseColor";
            this.labelChooseColor.Size = new System.Drawing.Size(35, 35);
            this.labelChooseColor.TabIndex = 3;
            this.labelChooseColor.Click += new System.EventHandler(this.labelChooseColor_Click);
            // 
            // labelChooseWidth
            // 
            this.labelChooseWidth.AutoSize = true;
            this.labelChooseWidth.Location = new System.Drawing.Point(152, 113);
            this.labelChooseWidth.Name = "labelChooseWidth";
            this.labelChooseWidth.Size = new System.Drawing.Size(13, 13);
            this.labelChooseWidth.TabIndex = 4;
            this.labelChooseWidth.Text = "1";
            // 
            // trackBarChooseWidth
            // 
            this.trackBarChooseWidth.LargeChange = 1;
            this.trackBarChooseWidth.Location = new System.Drawing.Point(15, 133);
            this.trackBarChooseWidth.Maximum = 15;
            this.trackBarChooseWidth.Minimum = 1;
            this.trackBarChooseWidth.Name = "trackBarChooseWidth";
            this.trackBarChooseWidth.Size = new System.Drawing.Size(257, 45);
            this.trackBarChooseWidth.TabIndex = 5;
            this.trackBarChooseWidth.Value = 1;
            this.trackBarChooseWidth.Scroll += new System.EventHandler(this.trackBarChooseWidth_Scroll);
            // 
            // buttonApply
            // 
            this.buttonApply.Location = new System.Drawing.Point(15, 185);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.Size = new System.Drawing.Size(257, 23);
            this.buttonApply.TabIndex = 6;
            this.buttonApply.Text = "Применить и выйти";
            this.buttonApply.UseVisualStyleBackColor = true;
            this.buttonApply.Click += new System.EventHandler(this.buttonApply_Click);
            // 
            // buttonCansel
            // 
            this.buttonCansel.Location = new System.Drawing.Point(15, 214);
            this.buttonCansel.Name = "buttonCansel";
            this.buttonCansel.Size = new System.Drawing.Size(257, 23);
            this.buttonCansel.TabIndex = 7;
            this.buttonCansel.Text = "Отменить и выйти";
            this.buttonCansel.UseVisualStyleBackColor = true;
            this.buttonCansel.Click += new System.EventHandler(this.buttonCansel_Click);
            // 
            // FormPenSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.buttonCansel);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.trackBarChooseWidth);
            this.Controls.Add(this.labelChooseWidth);
            this.Controls.Add(this.labelChooseColor);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormPenSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pen Setings";
            this.Load += new System.EventHandler(this.FormPenSettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBarChooseWidth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelChooseColor;
        private System.Windows.Forms.Label labelChooseWidth;
        private System.Windows.Forms.TrackBar trackBarChooseWidth;
        private System.Windows.Forms.Button buttonApply;
        private System.Windows.Forms.Button buttonCansel;
        private System.Windows.Forms.ColorDialog colorDialog;
    }
}