﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPaint
{
    public partial class FormTextSettings : Form
    {
        private Font font, chooseFont;
        private SolidBrush brush;

        public FormTextSettings(Font inputFont, SolidBrush inputBrush)
        {
            InitializeComponent();

            font = inputFont;
            brush = inputBrush;
        }

        private void FormTextSettings_Load(object sender, EventArgs e)
        {
            labelChooseColor.BackColor = brush.Color;
            labelChooseFont.Text = font.ToString();

            fontDialog.Font = (Font)font.Clone();
            chooseFont = fontDialog.Font;    
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void labelChooseColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = labelChooseColor.BackColor;
            if(colorDialog.ShowDialog() == DialogResult.OK)
            {
                labelChooseColor.BackColor = colorDialog.Color;
            }
        }

        private void buttonChooseFont_Click(object sender, EventArgs e)
        {
            if(fontDialog.ShowDialog() == DialogResult.OK)
            {
                chooseFont = fontDialog.Font;
                labelChooseFont.Text = chooseFont.ToString();
            }
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            brush.Color = labelChooseColor.BackColor;
            font = chooseFont;
            this.Tag = font;

            this.Close();
        }
    }
}
