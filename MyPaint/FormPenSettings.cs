﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPaint
{
    public partial class FormPenSettings : Form
    {
        private Pen pen;

        public FormPenSettings(Pen inputPen)
        {
            InitializeComponent();

            pen = inputPen;
        }

        private void FormPenSettings_Load(object sender, EventArgs e)
        {
            labelChooseColor.BackColor = pen.Color;
            labelChooseWidth.Text = pen.Width.ToString();
            trackBarChooseWidth.Value = (int)pen.Width;            
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void labelChooseColor_Click(object sender, EventArgs e)
        {
            colorDialog.Color = labelChooseColor.BackColor;
            if(colorDialog.ShowDialog() == DialogResult.OK)
            {
                labelChooseColor.BackColor = colorDialog.Color;
            }
        }

        private void trackBarChooseWidth_Scroll(object sender, EventArgs e)
        {
            labelChooseWidth.Text = trackBarChooseWidth.Value.ToString();
        }

        private void buttonApply_Click(object sender, EventArgs e)
        {
            pen.Color = labelChooseColor.BackColor;
            pen.Width = int.Parse(labelChooseWidth.Text);
            this.Close();
        }
    }
}
