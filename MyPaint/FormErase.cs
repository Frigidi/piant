﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPaint
{
    public partial class FormErase : Form
    {
        private Pen erase;

        public FormErase(Pen inputPen)
        {
            InitializeComponent();

            erase = inputPen;
        }

        private void Wash_Load(object sender, EventArgs e)
        {
            labelChooseWidth.Text = erase.Width.ToString();
            trackBarChooseWidth.Value = (int)erase.Width;
        }

        private void trackBarChooseWidth_Scroll(object sender, EventArgs e)
        {
            labelChooseWidth.Text = trackBarChooseWidth.Value.ToString();
        }

        private void buttonCansel_Click(object sender, EventArgs e)
        {
            Close();
        }
                
        private void buttonApply_Click(object sender, EventArgs e)
        {
            erase.Width = int.Parse(labelChooseWidth.Text);
            Close();
        }
    }
}
