﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyPaint
{
    public partial class FormHelp : Form
    {
        public FormHelp()
        {
            InitializeComponent();
        }

        private void FormHelp_Load(object sender, EventArgs e)
        {
            label2.Text = "My Paint — многофункциональный, но в то же время довольно простой в использовании растровый графический редактор.";
            label3.Text = "Недостатки: \n Нет возможности, создавая изображение, указать его размер. \n Нет возможности залить градиентом. \n Нет возможности поворота изображения. \n Нет поддержки прозрачности. \n Нет слоёв.";
        }
    }
}
